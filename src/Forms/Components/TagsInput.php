<?php

namespace KDA\Filament\Taggable\Forms\Components;

use Closure;
use Filament\Forms\Components\TagsInput as ComponentsTagsInput;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use KDA\Filament\Taggable\Concerns\HasGroup;
use KDA\Taggable\Facades\Tags;
use Spatie\Tags\Tag;

class TagsInput extends ComponentsTagsInput
{

    use HasGroup;
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadStateFromRelationshipsUsing(static function (TagsInput $component, ?Model $record): void {
            if (! method_exists($record, 'tagsWithType')) {
                return;
            }
            $group = $component->getGroup();
            $tags = $record->load('tags')->tagsWithType($group);

            $component->state($tags->pluck('translation')->toArray());
        });

        $this->saveRelationshipsUsing(static function (TagsInput $component, ?Model $record, array $state) {
            if (! (method_exists($record, 'syncTagsWithType') && method_exists($record, 'syncTags'))) {
                return;
            }

            if ($group = $component->getGroup()) {
                $record->syncTagsWithType($state, $group);

                return;
            }

            $record->syncTags($state);
        });

       $this->dehydrateStateUsing(static function (TagsInput $component, $state) {
            /*if ($separator = $component->getSeparator()) {
                return implode($separator, $state);
            }
*/
            $state = collect($state)->map(function($tag) use ($component){
                $class = Tags::getTagClassName();
                return $class::findOrCreate($tag,$component->getGroup(),app()->getLocale())->getKey();
                
            })->toArray();
                
            
            
            return $state;
        });

        $this->dehydrated(false);
    }

  
    public function getSuggestions(): array
    {
        if ($this->suggestions !== null) {
            return parent::getSuggestions();
        }

        $model = $this->getModel();
        $tagClass = $model ? Tags::getTagClassName() :null;
        $group = $this->getGroup();

        return $tagClass::query()
            ->when(
                filled($group),
                fn (Builder $query) => $query->where('group', $group),
                fn (Builder $query) => $query->where('group', null),
            )
            ->get()
            ->pluck('translation')
            ->toArray();
    }

}