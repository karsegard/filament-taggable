<?php

namespace KDA\Filament\Taggable\Forms\Components;


use Filament\Forms\Components\Field;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use KDA\Filament\Taggable\Concerns\HasGroup;
use KDA\Taggable\Facades\Tags;

class SelectTag extends Select
{
    use HasGroup;
    
    protected function setUp(): void
    {
        parent::setUp();
        
        $this->options(fn()=>Tags::forGroup($this->getGroup())->pluck('translation','id'));
        $this->saveRelationshipsUsing(function($record,$state){
            Tags::clearTags($record,$this->getGroup());
            Tags::syncExistingTagsIdsWithType($record,[$state],$this->getGroup());
        });
        $this->afterStateHydrated(function($component,$record){
            if($record){
                $tag = $record->tagsWithType($this->getGroup())->first();
                $component->state($tag?->getKey());
            }
        });
       
        $this->dehydrated(false);            
    }
}
