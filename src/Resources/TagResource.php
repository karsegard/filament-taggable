<?php

namespace KDA\Filament\Taggable\Resources;

use KDA\Filament\Taggable\Resources\TagResource\Pages;
use KDA\Filament\Taggable\Resources\TagResource\RelationManagers\ChildrenRelationManager;
use KDA\Filament\Taggable\Resources\TagResource\RelationManagers\TranslationsRelationManager;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Toggle;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use KDA\Taggable\Models\Tag;

class TagResource extends Resource
{
    protected static ?string $model = Tag::class;
    protected static bool $shouldRegisterNavigation = false;
    protected static ?string $navigationIcon = 'heroicon-o-collection';

    protected static ?string $group = null;

    public static function getResourceFormSchema():array{
        return [
             //
             TextInput::make('name')->required(),
             Toggle::make('new_group')->reactive()->dehydrated(true),
             Select::make('group')->options(static::getGroups())->visible(fn ($get) => static::$group == null && $get('new_group') == false),
             TextInput::make('group')->visible(fn ($get) => static::$group == null && $get('new_group') == true),
        ];
    }
    public static function form(Form $form): Form
    {
        return $form
            ->schema(static::getResourceFormSchema());
    }

    public static function getGroups()
    {
        return Tag::select('group')->distinct()->get()->pluck('group', 'group')->toArray();
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('name')->searchable(),
                TextColumn::make('group'),
                TextColumn::make('translations_count')->counts('translations'),
            ])
            ->filters([
                SelectFilter::make('group')->options(static::getGroups()),
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
            ChildrenRelationManager::class,

            TranslationsRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTags::route('/'),
            'create' => Pages\CreateTag::route('/create'),
            'edit' => Pages\EditTag::route('/{record}/edit'),
        ];
    }
}
