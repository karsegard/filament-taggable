<?php

namespace KDA\Filament\Taggable\Resources\TagResource\Pages;

use KDA\Filament\Taggable\Resources\TagResource;
use KDA\Filament\Taggable\Resources\TagResource\Pages\Breadcrumbs\TagBreadcrumb;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;

class EditTag extends EditRecord
{
    use TagBreadcrumb;

    protected static string $resource = TagResource::class;

    protected function getBreadcrumbs(): array
    {
        return $this->getCustomBreadcrumbs();
    }

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
        ];
    }
}
