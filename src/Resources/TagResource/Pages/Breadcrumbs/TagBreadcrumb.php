<?php

namespace KDA\Filament\Taggable\Resources\TagResource\Pages\Breadcrumbs;

use KDA\Filament\Taggable\Resources\TagResource;

trait TagBreadcrumb
{
    protected function getCustomBreadcrumbs(): array
    {
        $resource = static::getResource();
        $breadcrumbs = [];
        /*if ($this->record?->project) {
            $breadcrumbs[ProjectResource::getUrl('edit', ['record' => $this->record->project])] = $this->record->project->name;
        } else {
            $breadcrumbs[$resource::getUrl()] = $resource::getBreadcrumb();
        }

        if ($this->record?->sprint) {
            $breadcrumbs[SprintResource::getUrl('edit', ['record' => $this->record->sprint])] = $this->record->sprint->name;
        }
*/
        if ($this->record) {
            $ancestors = $this->record->ancestors;
            foreach ($ancestors as $ancestor) {
                $breadcrumbs[TagResource::getUrl('edit', ['record' => $ancestor])] = '(#'.$ancestor->id.') '.$ancestor->name;
            }
            $breadcrumbs[] = '(#'.$this->record->id.') '.$this->record->name;
        }
        $breadcrumbs[] = $this->getBreadcrumb();

        return $breadcrumbs;
    }
}
