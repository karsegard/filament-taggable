<?php

namespace KDA\Filament\Taggable\Resources\TagResource\Pages;

use KDA\Filament\Taggable\Resources\TagResource;
use Filament\Resources\Pages\CreateRecord;

class CreateTag extends CreateRecord
{
    protected static string $resource = TagResource::class;
}
