<?php

namespace KDA\Filament\Taggable\Resources\TagResource\Pages;

use KDA\Filament\Taggable\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTags extends ListRecords
{
    protected static string $resource = TagResource::class;

    public static function getEloquentQuery(): Builder
    {
        return parent::getEloquentQuery()->withoutParent();
    }

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
        ];
    }

    protected function shouldPersistTableFiltersInSession(): bool
    {
        return true;
    }
}
