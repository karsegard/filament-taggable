<?php

namespace KDA\Filament\Taggable;
use Filament\PluginServiceProvider;
use KDA\Filament\Taggable\Resources\TagResource;
use Spatie\LaravelPackageTools\Package;
use Livewire\Livewire;

class FilamentServiceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
           TagResource::class
    ];

    protected array $relationManagers = [
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-taggable');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        //Livewire::component('filament-media-manager-modal',\KDA\Filament\MediaManager\Livewire\MediaItemModal::class);
    }
}
