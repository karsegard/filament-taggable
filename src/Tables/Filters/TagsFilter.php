<?php

namespace KDA\Filament\Taggable\Tables\Filters;

use Closure;
use Filament\Forms\Components\Select;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use KDA\Taggable\Facades\Tags;

class TagsFilter extends SelectFilter{

    protected string $group = '';
    protected string $relationship;

    public function group(string $group='') : static{
        $this->group = $group;
        return $this;
    }

    public function getGroup():string{
        return $this->group;
    }

    public function relationship(string $relationshipName, string $titleColumnName = null, Closure $callback = null): static
    {
        $this->attribute("{$relationshipName}.{$titleColumnName}");

        if($callback){
           $this->query( $callback);
        }else{
            $this->query(function (Builder $query, array $data,$filter) use ($relationshipName): Builder {
                $values = $data['values']??false;
                $group = $filter->getGroup();
    
                if($values){
                    return $query->whereHas($relationshipName,function($q) use($values,$group){
                        $q->withAnyTag($values,$group);
                    });
                 }
                return $query;
            });
        }
        return $this;
    }

    protected function setUp(): void
    {
        parent::setUp();
        
        $this->options(function(){
            return Tags::forGroup($this->getGroup())->pluck('translation','id');
        });
        $this->query(function (Builder $query, array $data): Builder {
            $value = $data['value']??false;
            $values = $data['values']??false;
            $group = $this->getGroup();

            if($value){
               return $query->withTag($value,$group);
            }
            if($values){
                return $query->withAnyTag($values,$group);
             }
            return $query;
        });
    }

}