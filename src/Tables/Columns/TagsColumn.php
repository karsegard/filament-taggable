<?php

namespace KDA\Filament\Taggable\Tables\Columns;

use Filament\Tables\Columns\Concerns\HasColor;
use Filament\Tables\Columns\Concerns\HasIcon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Filament\Tables\Columns\TagsColumn as FilamentTagsColumn;
use KDA\Taggable\Facades\Tags;
use KDA\Taggable\Models\Tag;
use Illuminate\Database\Eloquent\Relations\Relation;

class TagsColumn extends FilamentTagsColumn
{
    use HasIcon;
    use HasColor;
    protected string $view = 'filament-taggable::columns.tags-column';
    protected ?string $type = null;

    public function getTags(): array
    {
        $state = $this->getState();
        if ($state && (! $state instanceof Collection) && (! $state instanceOf Tag)) {
            return $state;
        }

        $record = $this->getRecord();

        if ($this->queriesRelationships($record)) {
            $record = $record->getRelationValue($this->getRelationshipName());
        }

       /* if (! method_exists($record, 'tagsWithType')) {
            return [];
        }
*/
        $type = $this->getType();
        $tags = Tags::tagsWithType($record,$type);

        return $tags->pluck('translation')->toArray();
    }

    public function type(?string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function applyEagerLoading(Builder | Relation $query): Builder | Relation
    {
        if ($this->isHidden()) {
            return $query;
        }

        if ($this->queriesRelationships($query->getModel())) {
            return $query->with(["{$this->getRelationshipName()}.tags"]);
        }

        return $query->with(['tags']);
    }
}