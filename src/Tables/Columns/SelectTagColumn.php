<?php

namespace KDA\Filament\Taggable\Tables\Columns;

use Filament\Tables\Columns\SelectColumn;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Filament\Tables\Columns\TagsColumn as FilamentTagsColumn;
use KDA\Filament\Taggable\Concerns\HasGroup;
use KDA\Taggable\Facades\Tags;

class SelectTagColumn extends SelectColumn
{

    use HasGroup;
    protected function setUp(): void
    {
        parent::setUp();

        $this->disableClick();
        $this->options(fn () => Tags::forGroup($this->getGroup())->pluck('translation', 'id'));
        $this->placeholder(__('forms::components.select.placeholder'));
        $this->updateStateUsing(function ($record, $state) {
            Tags::clearTags($record, $this->getGroup());
            Tags::syncExistingTagsIdsWithType($record, [$state], $this->getGroup());
            return $state;
        });
        /* $this->getStateUsing(function($record){
            if($record){
                $tag = $record->tagsWithType($this->getGroup())->first();
                //component->state($tag?->getKey());
                return $tag?->getKey();
            }
            return null;
        });*/
    }
    protected function getStateFromRecord()
    {
        $record = $this->getRecord();
        $tag = $record->tagsWithType($this->getGroup())->first();
        //component->state($tag?->getKey());
        return $tag?->getKey();
    }
}
