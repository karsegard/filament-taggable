# This is my package filament-taggable

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/filament-taggable.svg?style=flat-square)](https://packagist.org/packages/kda/filament-taggable)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/filament-taggable.svg?style=flat-square)](https://packagist.org/packages/kda/filament-taggable)

## Installation

You can install the package via composer:

```bash
composer require kda/filament-taggable
```


## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/fdt2k/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Fabien Karsegard](https://github.com/fdt2k)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
