@php
    $state = $getTags();
    $icon = $getIcon();
    $iconPosition = $getIconPosition();
    $iconClasses = 'w-4 h-4 flex-none';
@endphp

<div class="flex flex-row items-center">
    @if ($icon && $iconPosition === 'before')
        <x-dynamic-component
            :component="$icon"
            :class="$iconClasses"
        />
    @endif
    <div
        {{ $attributes->merge($getExtraAttributes())->class([
            'filament-tables-tags-column flex flex-wrap items-center gap-1',
            'px-4 py-3' => !$isInline(),
            match ($getAlignment()) {
                'start' => 'justify-start',
                'center' => 'justify-center',
                'end' => 'justify-end',
                'left' => 'justify-start rtl:flex-row-reverse',
                'center' => 'justify-center',
                'right' => 'justify-end rtl:flex-row-reverse',
                default => null,
            },
            
        ]) }}>

        @foreach (array_slice($getTags(), 0, $getLimit()) as $tag)
            <span @class([
                'inline-flex items-center justify-center min-h-6 px-2 py-0.5 text-sm font-medium tracking-tight rounded-xl  whitespace-normal',
                'dark:text-primary-500' => config('tables.dark_mode'),
                match ($getColor()) {
                    'danger' => 'text-danger-600 bg-danger-500/10',
                    'primary' => 'text-primary-600 bg-primary-500/10',
                    'secondary' => 'text-gray-500 bg-gray-500/10',
                    'success' => 'text-success-600 bg-success-500/10',
                    'warning' => 'text-warning-600 bg-warning-500/10',
                    default => 'text-primary-700 bg-primary-500/10',
                }
            ])>
                {{ $tag }}
            </span>
        @endforeach

        @if ($hasActiveLimit())
            <span class="ml-1 text-xs">
                {{ trans_choice('tables::table.columns.tags.more', count($getTags()) - $getLimit()) }}
            </span>
        @endif

    </div>
    @if ($icon && $iconPosition === 'after')
        <x-dynamic-component
            :component="$icon"
            :class="$iconClasses"
        />
    @endif
</div>
